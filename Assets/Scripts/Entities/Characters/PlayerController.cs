﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Cinemachine;
using DG.Tweening;

[RequireComponent (typeof(PlatformerActorMovement))]
[RequireComponent (typeof(BouncingBomb))]
[RequireComponent (typeof(Health))]

public class PlayerController : MonoBehaviour {

  public KeyCode RightInput = KeyCode.RightArrow;
  public KeyCode LeftInput = KeyCode.LeftArrow;
  public KeyCode UpInput = KeyCode.UpArrow;
  public KeyCode DownInput = KeyCode.DownArrow;
  public KeyCode JumpInput = KeyCode.Space;
  public KeyCode BombInput = KeyCode.Z;

  public CinemachineVirtualCamera PlayerCam;

  private PlatformerActorMovement _actorMovement;
  private BouncingBomb _bouncingBomb;
  private int _hInput;
  private CinemachineFramingTransposer _playerCamComposer;

  private void Awake() {
    _actorMovement = GetComponent<PlatformerActorMovement>();
    _bouncingBomb = GetComponent<BouncingBomb>();
    _playerCamComposer = PlayerCam.GetCinemachineComponent<CinemachineFramingTransposer>();
  }

  public void Update() {
    _actorMovement.IsRightInput = Input.GetKey(RightInput);
    _actorMovement.IsLeftInput = Input.GetKey(LeftInput);
    _actorMovement.IsJumpInput = Input.GetKey(JumpInput);
    _bouncingBomb.IsBombInput = Input.GetKeyDown(BombInput);
    _bouncingBomb.IsUpInput = Input.GetKey(UpInput);
    _bouncingBomb.IsDownInput = Input.GetKey(DownInput);

    //Blend multiple v cams together in the cinemachine brain to move between states when
    //player is bouncing/in the air when being blasted, and when everything is normal
    //Look ahead time for new camera will be 0. Dead Zone Width might need to be 0 too.
  }
}
