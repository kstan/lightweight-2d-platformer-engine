﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(BoxCollider2D))]

public class PlatformerActorMovement : PhysicsObject {

  public float HSpeed = 5;
  public float MaxHSpeed = 12;
  public float GravityScale = 8;
  public float Mass = 1f;
  public float JumpHeight = 30;
  public bool ShouldFreezeRotation = true;

  public bool IsRightInput { get; set; }
  public bool IsLeftInput { get; set; }
  public bool IsJumpInput { get; set; }
  public int FacingDirection { get; set; }

  private int _hInput;
  private bool _isFalling;
  private bool _hasJumped;

  public Vector2 GetPlayerPos() {
    return transform.position;
  }

  private void Start() {
    ReloadRigidbodySettings();
    FacingDirection = 1;
  }

  public void ReloadRigidbodySettings() {
    _rigidbody.mass = Mass;
		_rigidbody.gravityScale = GravityScale;
		_rigidbody.freezeRotation = ShouldFreezeRotation;
  }

  private void Update() {
    //Handling horizontal movement
    _hInput = Convert.ToInt32 (IsRightInput) - Convert.ToInt32 (IsLeftInput);
    if (_hInput != 0 && !IsHittingWall()) {
      //Add horizontal force while also clamping max horizontal speed
      _rigidbody.AddForce (new Vector2 (HSpeed * _hInput, 0), ForceMode2D.Impulse);
      float hSpeed = Mathf.Clamp (_rigidbody.velocity.x, -MaxHSpeed, MaxHSpeed);
      _rigidbody.velocity = new Vector2 (hSpeed, _rigidbody.velocity.y);
      FacingDirection = _hInput;
    } else if (!CanBounce){
      //Make player "slide" to a stop
      float hSpeed = Mathf.Lerp (_rigidbody.velocity.x, 0, 0.2f);
      _rigidbody.velocity = new Vector2 (hSpeed, _rigidbody.velocity.y);
    }

    //Handling vertical movement
    if (IsJumpInput && !_hasJumped && (IsGrounded () || IsOnPhysicsObject())) {
      _rigidbody.AddForce (new Vector2 (0, JumpHeight), ForceMode2D.Impulse);
      _isFalling = false;
      _hasJumped = true;
    } else if (_rigidbody.velocity.y < 0 && !_isFalling && !CanBounce) {
      //Falls fast
      _rigidbody.gravityScale = GravityScale * 3f;
      _isFalling = true;
    } else if (_rigidbody.velocity.y > 0 && !IsJumpInput && !_isFalling && !CanBounce) {
      //Shorter jump for short taps
      _rigidbody.gravityScale = GravityScale * 3f;
      _isFalling = true;
    } else if (!IsJumpInput && _hasJumped && (IsGrounded () || IsOnPhysicsObject())) {
      //Jump input can be registered again when landed and jump input is not pressed
      _hasJumped = false;
    } else {
      //Reset back to normal gravity
      _rigidbody.gravityScale = GravityScale;
      _isFalling = false;
    }
  }

  public void SetBounceFor(float seconds) {
    StartCoroutine("BounceFor", seconds);
  }

  public void StopBounce() {
    StopCoroutine("BounceFor");
  }

  private IEnumerator BounceFor(float seconds){
    CanBounce = true;
    yield return new WaitForSeconds(seconds);
    CanBounce = false;
  }
}
