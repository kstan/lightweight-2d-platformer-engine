﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

  public float InitialPoints = 100;
  public float Points { get; set; }

  private bool _isReceivingDamageOverTime;
  private bool _isReceivingHealingOverTime;
  private Coroutine _damageOverTimeCoroutine;
  private Coroutine _healingOverTimeCoroutine;

  public void Awake() {
    Points = InitialPoints;
  }

  public void ExecuteDamageOverTime(float damagePerSec, int time) {
    _damageOverTimeCoroutine = StartCoroutine(DamageOverTime(damagePerSec, time));
  }

  public void ExecuteHealingOverTime(float healingPerSec, int time) {
    _healingOverTimeCoroutine = StartCoroutine(HealingOverTime(healingPerSec, time));
  }

  public void StopDamageOverTime() {
    if (_damageOverTimeCoroutine != null) {
      StopCoroutine(_damageOverTimeCoroutine);
      _damageOverTimeCoroutine = null;
    }
  }

  public void StopHealingOverTime() {
    if (_healingOverTimeCoroutine != null) {
      StopCoroutine(_healingOverTimeCoroutine);
      _healingOverTimeCoroutine = null;
    }
  }

  private IEnumerator DamageOverTime(float damagePerSec, int time) {
    _isReceivingDamageOverTime = true;
    while (time > 0 && Points > 0 ) {
      yield return new WaitForSeconds(1);
      time--;
      ReceiveDamage(damagePerSec);
    }
    _isReceivingDamageOverTime = false;
  }

  private IEnumerator HealingOverTime(float healingPerSec, int time) {
    _isReceivingHealingOverTime = true;
    while (time > 0 && Points > 0 ) {
      yield return new WaitForSeconds(1);
      time--;
      ReceiveHealing(healingPerSec);
    }
    _isReceivingHealingOverTime = false;
  }

  public void ReceiveDamage(float points) {
    Points -= points;
  }

  public void ReceiveHealing(float points) {
    Points += points;
  }

  public float GetTotalDamageReceived() {
    return InitialPoints - Points;
  }

  public void ApplyHitStop(float duration) {
    StartCoroutine(HitStop(duration));
  }

  private IEnumerator HitStop(float duration) {
    Time.timeScale = 0f;
    float pauseEndTime = Time.realtimeSinceStartup + duration;
    while (Time.realtimeSinceStartup < pauseEndTime) yield return 0;
    Time.timeScale = 1;
  } 
}
