using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

//IMPORTANT DEV NOTE:
//Set the following Physics2D settings for more accurate physics behaviour
//Velocity Threshold: 10
//Default Contact Offset: 0.001
//Queries Start In Colliders: false

public class PhysicsObject : MonoBehaviour {

  public bool CanBounce;
  protected Rigidbody2D _rigidbody;
  protected Collider2D _collider;
  private Vector2 _velocityBeforeCollision;

  private void Awake() {
    _rigidbody = GetComponent<Rigidbody2D>();
    _collider = GetComponent<Collider2D>();
  }

  private void Start() {
    //Default rigidbody settings
    _rigidbody.drag = 3;
    _rigidbody.gravityScale = 8;
    _rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
  }

  private void FixedUpdate() {
    //Handle bounce behaviour
    //The method is to check for bomb velocity before collision, then using that velocity
    //to find the "reflectVelocity" on the next moment there is contact with a surface.
    if (CanBounce) {
      RaycastHit2D hit = Physics2D.Raycast(_rigidbody.position, _rigidbody.velocity);
      if (hit.distance < 1f && hit.collider != null) {
        _velocityBeforeCollision = _rigidbody.velocity;
      }
      Collider2D other = Physics2D.OverlapArea(_collider.bounds.min, _collider.bounds.max, LayerMask.GetMask(Layers.Player, Layers.Terrain, Layers.Enemy, Layers.PhysicsObject));
      if (other != null) {
        ContactPoint2D[] contacts = new ContactPoint2D[20];
        int contactCount = _collider.GetContacts(contacts);
        ContactPoint2D contact = contacts[0];
        if (contact.collider != null) {
          hit = Physics2D.Raycast(_rigidbody.position, _velocityBeforeCollision);
          if (hit.collider == contact.collider) {
            Vector2 reflectVelocity = Vector2.Reflect(_velocityBeforeCollision, hit.normal);
            _rigidbody.velocity += reflectVelocity;
            _velocityBeforeCollision = Vector2.zero;
          }
        }
      }
    }
  }

  public bool IsGrounded() {
    Vector2 overlapCentre = transform.position;
    overlapCentre.y -= _collider.bounds.size.y * 0.5f;
    Vector2 overlapSize = new Vector2(_collider.bounds.size.x, 0.1f);
    return Physics2D.OverlapBox(overlapCentre, overlapSize, 0, LayerMask.GetMask(Layers.Terrain));
  }

  protected bool IsHittingWall() {
		Vector2 overlapCentre = transform.position;
		overlapCentre.x += _collider.bounds.size.x * 0.5f * Math.Sign(_rigidbody.velocity.x);
		Vector2 overlapSize = new Vector2 (0.1f, _collider.bounds.size.y * 0.9f);
		return Physics2D.OverlapBox(overlapCentre, overlapSize, 0, LayerMask.GetMask(Layers.Terrain));
	}

  public bool IsOnPhysicsObject() {
    Vector2 overlapCentre = transform.position;
    overlapCentre.y -= _collider.bounds.size.y * 0.5f;
    Vector2 overlapSize = new Vector2(_collider.bounds.size.x, 0.1f);
    Collider2D other = Physics2D.OverlapBox(overlapCentre, overlapSize, 0, LayerMask.GetMask(Layers.PhysicsObject));
    if (other!=null) if (other.isTrigger == false) return true;
    return false;
  }
}