﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using DG.Tweening;

[RequireComponent (typeof(Rigidbody2D))]
[RequireComponent (typeof(BoxCollider2D))]
[RequireComponent (typeof(PlatformerActorMovement))]

public class BouncingBomb : MonoBehaviour {

  public bool IsBombInput { get; set; }
  public bool IsUpInput { get; set; }
  public bool IsDownInput { get; set; }
  public GameObject BombPrefab;
  public float BombRadius = 2;
  public GameObject BlastEffect;
  public float BombDamage = 10;
  public float BlastSpeed = 35;

  private float _bombCarryPosXOffset = 0.7f;
  private float _bombCarryPosYOffset = 0f;
  private PlatformerActorMovement _actorMovement;
  private GameObject _bombObject;
  private Rigidbody2D _bombRb;
  private CircleCollider2D _bombCollider;
  private PhysicsObject _physicsObject;
  private string _bombState;
  private bool _canCollideWithPlayer;
  private Vector2 _bombVelocityBeforeCollision;
  private GameObject _blastEffect;

  private void Start() {
    _actorMovement = GetComponent<PlatformerActorMovement>();
    _bombObject = Instantiate(BombPrefab);
    _bombRb = _bombObject.GetComponent<Rigidbody2D>();
    _bombCollider = _bombObject.GetComponent<CircleCollider2D>();
    _bombRb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
    _physicsObject = _bombObject.GetComponent<PhysicsObject>();
    _blastEffect = Instantiate(BlastEffect);
    _blastEffect.SetActive(false);
    if (_physicsObject == null) _physicsObject = _bombObject.AddComponent<PhysicsObject>();
    KeepBomb();
  }

  private void Update() {
    //Handle bomb related actions
    if (IsBombInput) {
      //Simple "state-machine" to keep track of bomb states
      if (_bombState == BombStates.Kept) {
        CarryBomb();
      } else if (_bombState == BombStates.Carry) {
        ThrowBomb();
      } else if (_bombState == BombStates.Thrown) {
        ExplodeBomb();
        KeepBomb();
      }
    } else if (_bombState == BombStates.Carry) {
      //Update carry position
      CarryBomb();
    }
  }

  private void KeepBomb() {
    if (_bombObject != null) {
      if (_bombObject.transform.parent != transform) _bombObject.transform.SetParent(transform);
      if (_bombObject.activeSelf) _bombObject.SetActive(false);
      _bombCollider.isTrigger = true;
      _bombState = BombStates.Kept;
      _physicsObject.CanBounce = false;
    }
  }

  private void ThrowBomb() {
    if (_bombObject != null) {
      Transform parent = _bombObject.transform.parent;
      if (parent != null) _bombObject.transform.SetParent(null);
      if (!_bombObject.activeSelf) _bombObject.SetActive(true);
      if (IsUpInput) {
        _bombRb.transform.position = new Vector2(parent.position.x, parent.position.y + 0.5f);
        _bombRb.AddForce (new Vector2 (0, 35), ForceMode2D.Impulse);
      } else if (IsDownInput) {
        _bombRb.transform.position = new Vector2(parent.position.x, parent.position.y - 0.5f);
        _bombRb.AddForce (new Vector2 (0, -10), ForceMode2D.Impulse);
      } else {
        _bombRb.position = new Vector2(_bombRb.position.x, _bombRb.position.y + 0.5f);
        _bombRb.AddForce (new Vector2 (20 * _actorMovement.FacingDirection, 20), ForceMode2D.Impulse);
      }
      _bombCollider.isTrigger = false;
      _bombState = BombStates.Thrown;
      _physicsObject.CanBounce = true;
    }
  }

  private void CarryBomb() {
    if (_bombObject != null) {
      if (_bombObject.transform.parent != transform) _bombObject.transform.SetParent(transform);
      if (!_bombObject.activeSelf) _bombObject.SetActive(true);
      _bombObject.transform.localPosition = new Vector2(_bombCarryPosXOffset * _actorMovement.FacingDirection, _bombCarryPosYOffset);
      _bombState = BombStates.Carry;
    }
  }

  private void ExplodeBomb() {
    EffectsManager.instance.SpawnBombDissolve(_bombObject.transform.position);
    Camera.main.transform.DOShakePosition(0.2f, 0.1f, 20, 90, false, false);
    //Move objects in blast radius
    Collider2D[] others = Physics2D.OverlapCircleAll(_bombRb.position, BombRadius, LayerMask.GetMask(Layers.Player, Layers.Enemy, Layers.PhysicsObject));
    for (int i = 0; i < others.Length; i++) {
      Collider2D other = others[i];
      Rigidbody2D otherRb = other.GetComponent<Rigidbody2D>();
      if (otherRb != null && otherRb.gameObject != _bombObject) {
        float impactMultiplier = 1;
        if (other.GetComponent<Health>() != null) {
          Health otherHealth = other.GetComponent<Health>();
          otherHealth.ReceiveDamage(BombDamage);
          //Calculate impact multiplier based on damage received
          impactMultiplier += otherHealth.GetTotalDamageReceived()/100;
        }
        //Make the platform actor bounce for a while from the force of the bomb
        if (other.GetComponent<PlatformerActorMovement>() != null) {
          other.GetComponent<PlatformerActorMovement>().SetBounceFor(impactMultiplier);
        }
        //Blast the physics object in a certain direction
        if (other.GetComponent<PhysicsObject>() != null) {
          Vector2 blastDirection = (otherRb.position - _bombRb.position).normalized;
          if (other.GetComponent<PhysicsObject>().IsGrounded())
            blastDirection.y += 1;
          otherRb.velocity += blastDirection * BlastSpeed * impactMultiplier;
        }
      }
    }
  }
}
