﻿public static class Layers {
	public static string Player = "Player";
	public static string Terrain = "Terrain";
	public static string PhysicsObject = "PhysicsObject";
	public static string Enemy = "Enemy";
}

public static class BombStates {
    public static string Kept = "Kept";
    public static string Carry = "Carry";
    public static string Thrown = "Thrown";
}

public static class Effects {
	public const string BombDissolve = "BombDissolve";
	public const string Fire = "Fire";
}