using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EffectsManager : MonoBehaviour {

  [SerializeField]
  private PoolItemsScriptableObject poolEffects;

  public static EffectsManager instance;

  private void Awake() {
    if (instance == null) {
      instance = this;
    }
    poolEffects.Initialize();
  }

  public void SpawnBombDissolve(Vector2 position) {
    GameObject effect = poolEffects.Spawn(Effects.BombDissolve, position);
    effect.GetComponent<ParticleSystem>().Play();
    float time = 0.6f;
    DOTween.To(()=> time, x=> time = x, 0, 0.5f).SetEase(Ease.OutFlash)
    .OnUpdate(()=>{
      effect.GetComponent<SpriteRenderer>().material.SetFloat("_time", time);
    }).OnComplete(()=>{
      effect.GetComponent<ParticleSystem>().Stop();
      effect.SetActive(false);
    });
  }

}
