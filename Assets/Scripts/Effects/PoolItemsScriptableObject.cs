﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZObjectPools;

[CreateAssetMenu(fileName = "PoolItemsScriptableObject", menuName = "Pool Items Scriptable Object", order = 51)]
public class PoolItemsScriptableObject : ScriptableObject {

  [System.Serializable]
  public class PoolItem {
    public string Name;
    public GameObject Prefab;
    public int PoolSize;
  }

  [SerializeField]
  private List<PoolItem> PoolItems = new List<PoolItem> ();

  private Dictionary<string, EZObjectPool> _pools;

  public void Initialize() {
    _pools = new Dictionary<string, EZObjectPool>();
    for (int i = 0; i < PoolItems.Count; i++) {
      PoolItem poolItem = PoolItems[i];
      EZObjectPool pool = EZObjectPool.CreateObjectPool(poolItem.Prefab, poolItem.Name, poolItem.PoolSize, true, true, true);
      _pools.Add(poolItem.Name, pool);
    }
  }

  public virtual GameObject Spawn(string name, Vector2 position) {
    GameObject nextObject;
    EZObjectPool pool = _pools[name];
    pool.TryGetNextObject(position, Quaternion.identity, out nextObject);
    return nextObject;
  }

}
