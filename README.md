# Lightweight 2D Platformer Engine

## Introduction

This is a light weight 2D platformer engine. This engine should be used for small projects / quick prototypes only.

This engine is built on loosely coupled components and few layers of abstraction, so that the project is always easy to be picked up and modified by new developers. 

## Features

1.  `PhysicsObject.cs`: Base class for physics object that bounces off smooth surfaces based on raycast calculations.

2. `PlatformerActorMovement.cs`: Physics-based 2D character controller that can be used for both player and AI characters. Movement can be controlled by an inherited or external component (e.g. look at `PlayerController.cs`), by setting the `Is[Right/Left/Jump]Input()` public boolean functions.

3. `Health.cs`: Basic health system with effects such as damage over time, healing over time, hitstop.

4. `PoolItemsScriptableObject.cs`: Basic object pooling system using scriptable objects. In the Unity editor, RIGHT CLICK in the `Projects` tab/window > Create > Pool Items Scriptable Object. You may then reference this scriptable object in a "manager" script to initialise the object pool and spawn/destroy objects from there (e.g. `EffectsManager.cs`).

## List of Settings and Packages Being Used

1. Packages
    - Install Unity Shader Graph
    - Install Lightweight Rendering Pipeline
    - Install Cinemachine

2. Non-default Project Settings:
    - Velocity Threshold = 10
    - Default Contact Offset = 0.001
    - Queries Start In Colliders = false
    - Fixed Timestep = 0.01
    - Maximum Allowed Timesteo = 0.05

3. Non-default Rigidbody2D settings for PhysicsObject
    - Gravity Scale = 8
    - Linear Drag = 3

4. Pixels Per Unit = 100

## Third-party Plugins and Assets

1. [DOTween](http://dotween.demigiant.com/documentation.php) to easily tween game objects and UI elements.

2. [EZ Object Pool](https://assetstore.unity.com/packages/tools/ez-object-pools-28002) for easy implementation of object pools.

4. Speech bubble + dialogue system. Review one of these solutions: [Yarn Spindle](https://github.com/thesecretlab/YarnSpinner), [FancySpeechBubble](https://github.com/solosodium/FancySpeechBubble), [BubbleGum](https://github.com/lerenwe/BubbleGum), and [UnityTextTyper](https://github.com/redbluegames/unity-text-typer).

